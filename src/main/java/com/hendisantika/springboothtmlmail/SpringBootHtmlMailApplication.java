package com.hendisantika.springboothtmlmail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootHtmlMailApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootHtmlMailApplication.class, args);
    }
}
